lazy val baseName = "TreeTable"
lazy val baseNameL = baseName.toLowerCase

lazy val projectVersion = "1.6.3"
lazy val mimaVersion    = "1.6.0"

name := baseName

// ---- dependencies ----

lazy val deps = new {
  val main = new {
    val swingPlus = "0.5.0"
  }
  val test = new {
    val submin    = "0.3.5"
  }
}

def basicJavaOpts = Seq("-source", "1.8")

// sonatype plugin requires that these are in global
ThisBuild / version       := projectVersion
ThisBuild / organization  := "de.sciss"
ThisBuild / versionScheme := Some("pvp")

lazy val commonSettings = Seq(
  scalaVersion       := "2.13.7",
  crossScalaVersions := Seq("3.1.0", "2.13.7", "2.12.15"),
  javacOptions                   := basicJavaOpts ++ Seq("-encoding", "utf8", "-Xlint:unchecked", "-target", "1.8"),
  Compile / doc / javacOptions   := basicJavaOpts,  // doesn't eat `-encoding` or `target`
  scalacOptions     ++= Seq("-deprecation", "-Xlint"),
  description        := "A TreeTable component for Swing",
  homepage           := Some(url(s"https://github.com/Sciss/$baseName")),
  licenses           := Seq("LGPL v3+" -> url("https://www.gnu.org/licenses/lgpl-3.0.txt"))
) ++ publishSettings

// ---- publishing ----

lazy val publishSettings = Seq(
  publishMavenStyle := true,
  publishTo := {
    Some(if (isSnapshot.value)
      "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
    else
      "Sonatype Releases"  at "https://oss.sonatype.org/service/local/staging/deploy/maven2"
    )
  },
  Test / publishArtifact := false,
  pomIncludeRepository := { _ => false }
)

lazy val root = project.in(file("."))
  .aggregate(javaProject, scalaProject)
  .dependsOn(javaProject, scalaProject) // i.e. root = full sub project. if you depend on root, will draw all sub modules.
  .settings(commonSettings)
  .settings(
    name              := baseNameL,
    packagedArtifacts := Map.empty           // prevent publishing anything!
  )

lazy val javaProject = project.withId(s"$baseNameL-java").in(file("java"))
  .settings(commonSettings)
  .settings(
    name             := s"$baseName-java",
    autoScalaLibrary := false,
    crossPaths       := false,
    Compile / javacOptions ++= Seq("-g", "-target", "1.8", "-source", "1.8"),
    Compile / doc / javacOptions := Nil,  // yeah right, ...
//    publishArtifact := {
//      val old = publishArtifact.value
//      old && scalaVersion.value.startsWith("2.13")  // only publish once when cross-building
//    },
    pomExtra := pomExtraBoth,
    mimaPreviousArtifacts := Set("de.sciss" % s"$baseNameL-java" % mimaVersion)
  )

lazy val scalaProject = project.withId(s"$baseNameL-scala").in(file("scala"))
  .dependsOn(javaProject)
  .settings(commonSettings)
  .settings(
    name             := s"$baseName-scala",
    libraryDependencies ++= Seq(
      "de.sciss" %% "swingplus" % deps.main.swingPlus,
      "de.sciss" %  "submin"    % deps.test.submin % Test
    ),
    pomExtra := pomBase ++ pomDevsSciss,
    mimaPreviousArtifacts := Set("de.sciss" %% s"$baseNameL-scala" % mimaVersion)
  )

def pomExtraBoth = pomBase ++ pomDevsBoth

def pomBase =
  <scm>
    <url>git@github.com:Sciss/TreeTable.git</url>
    <connection>scm:git:git@github.com:Sciss/TreeTable.git</connection>
  </scm>

def pomDevSciss =
  <developer>
    <id>sciss</id>
    <name>Hanns Holger Rutz</name>
    <url>https://www.sciss.de</url>
  </developer>

def pomDevAephyr =
  <developer>
    <id>aephyr</id>
    <name>unknown</name>
    <url>http://code.google.com/p/aephyr/</url>
  </developer>

def pomDevsBoth =
  <developers>
    {pomDevSciss}
    {pomDevAephyr}
  </developers>

def pomDevsSciss =
  <developers>
    {pomDevSciss}
  </developers>

